<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>
-->

# About

GitLab CI Templates for the various REST/CRUD/IRC micro services written in
Go and/or Ruby and/or NodeJS.

# Use

e.g.

```
include:
  - project: sitter/gitlab-ci-service-templates
    file:
      - base.yml
      - dependency-scanning.yml
```

# Includes

- base.yml: base for most go-ish services. Includes go and node caching as well as base templates for `go test` as well
  as SSH setup for automatic deployment
- cache-go.yml: `.go-cache` for go related caching sets up env suitably so `go` looks for stuff in the right paths
- cache-node.yml: `.node-cache` same for node (no env meddling though)
- cache-ruby.yml: `.ruby-cache` same for ruby (partial env meddling - when dealing with `gem` paths may need passing explicitly still)
- dependency-scanning.yml: injects a build and deploy job that uses gitlab's gemnasium tool to generate a dependency
  report (unpatched CVEs and the like) that then gets deployed as a gitlab issue on the project if there are unpatched
  problems. Relies on https://invent.kde.org/sitter/gitlab-vulnerability-issues

# WARNING

Mind that CI includes may only be included once across the entire include tree. In particular that means you shouldn't
include the caches directly if you also want to use one of the other includes since they already include the caches.
