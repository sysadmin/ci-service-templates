#!/bin/sh
# SPDX-License-Identifier: BSD-3-Clause
# SPDX-FileCopyrightText: 2021-2022 Harald Sitter <sitter@kde.org>

# Only run as part of the CI!

set -e

# We need to construct an intermediate yml file similar to how our stuff would
# be used. Otherwise linting will fail because our files are only partial while
# the endpiont expects a complete file.
# For simplicity we only use the most high level ymls that then in turn include
# the lower level caching ymls.
cat << EOF > lint.yml
include:
  - project: ${CI_PROJECT_PATH}
    file:
      - base.yml
      - dependency-scanning.yml

foo:
  script: bar
EOF

CI_SERVER_URL="${CI_SERVER_URL:-https://invent.kde.org}"

node_modules/gitlab-ci-lint/bin/gitlab-ci-lint --url "$CI_SERVER_URL" --token "$KDECI_GITLAB_TOKEN" lint.yml
rm -f lint.yml
